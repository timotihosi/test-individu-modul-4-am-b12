| Nama                              | NRP        |
|-----------------------------------|------------|
|Arif Nugraha Santosa               |5025211048  |

## Test Individu Modul 4
### Soal 1

Buatlah sebuah _file_ konfigurasi **Dockerfile**. Dari file tersebut, isi sebuah _image_ dari **nginx** lalu beri informasi bahwa _image_ ini akan meng-_ekspose_ **port 80**. Build dengan nama **image-nginx versi v1.0** lalu _run_ dengan nama **container-nginx** serta me-_forward_ **port 80** dari _container_ ke **port 80** PC.

**Jawaban:**

1. Buatlah file Dockerfile yang berisi konfigurasi berikut
```Dockerfile
# Menggunakan image nginx sebagai base
FROM nginx

RUN apt-get update && apt-get upgrade -y

# Menginformasikan bahwa container akan meng-ekspose port 80
EXPOSE 80

# Build dengan nama image-nginx versi v1.0
LABEL version="v1.0"

# Menjalankan container dengan nama container-nginx
# Forward port 80 dari container ke port 80 PC
# Ubah `your_pc_port` sesuai dengan port yang ingin digunakan di PC Anda
# Jika ingin menggunakan port lain di PC, ganti angka 80 pada `your_pc_port`
# dengan port yang Anda inginkan
RUN echo "Forwarding port 80 from container to port 80 on PC"
CMD ["nginx", "-g", "daemon off;"]

```

2. melakukan build image dengan menjalankan perintah berikut di terminal:
```
sudo docker build -t image-nginx:v1.0 .
```
<img src="5025211048_Arif Nugraha Santosa/image/build.png" width ="550">


Perintah tersebut akan menghasilkan image dengan nama image-nginx dan versi v1.0.

3. Jalankan container dengan menjalankan perintah berikut di terminal:
```
sudo docker run -d --name container-nginx -p 80:80 image-nginx:v1.0
```
<img src="5025211048_Arif Nugraha Santosa/image/run.png" width ="550">

Perintah tersebut akan menjalankan container dengan nama container-nginx dan meneruskan (forwarding) port 80 dari container ke port 80.

akses server nginx yang berjalan di container melalui http://localhost pada browser.

4. Hasil localhost

<img src="5025211048_Arif Nugraha Santosa/image/hasil.png" width ="550">

### Soal 2

Pertama, buatlah Docker Network menggunakan _repository_ **https://github.com/dptsi/nginx-proxy**

Lalu, _pull repository_ **https://github.com/akunlainfiqi/hbd-clone-iyok** dan buatlah _container_-nya!

Lalu, buatlah _domain_ **hbdsayang.local** di _hosts_ agar mengarah ke _container_ tersebut!

**Jawaban:**

1. Clone repository nginx-proxy ke direktori kerja Anda dengan menjalankan perintah berikut di terminal:
```
git clone https://github.com/dptsi/nginx-proxy.git
```
<img src="5025211048_Arif Nugraha Santosa/image/1.png" width ="550">

2. Masuk ke direktori nginx-proxy yang baru dibuat:
```
cd nginx-proxy
```

3. Jalankan perintah docker network create untuk membuat Docker Network dengan nama nginx-proxy:
```
docker network create nginx-proxy-network
```
<img src="5025211048_Arif Nugraha Santosa/image/3.png" width ="550">


4. Jangan lupa untuk mengubah port pada docker-compose.yml menjadi port selain 80. Lalu lakukan command
```
docker-compose up
```

5. Clone repository hbd-clone-iyok ke direktori kerja Anda dengan menjalankan perintah berikut di terminal:
```
git clone https://github.com/akunlainfiqi/hbd-clone-iyok.git
```
<img src="5025211048_Arif Nugraha Santosa/image/5.png" width ="550">


5. Masuk ke direktori hbd-clone-iyok yang baru dibuat:
```
cd hbd-clone-iyok
```

6. Jalankan perintah docker-compose up -d untuk membuat dan menjalankan container menggunakan Docker Compose:
```
docker-compose up -d
```

7. Buka file hosts dengan menggunakan editor teks dengan hak akses administrator
```
sudo nano /etc/hosts
```

8. Memasukan IP berikut
```
127.0.0.1 hbdsayang.local
```

9. Masuk ke http://hbdsayang.local dan berikut hasilnya
<img src="5025211048_Arif Nugraha Santosa/image/hasil2.png" width ="550">


