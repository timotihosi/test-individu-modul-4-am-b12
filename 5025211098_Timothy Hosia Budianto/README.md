## **Soal 1**
Buat dockerfile dengan base image nginx yang mengekspose 8080. jalankan diport 8080:80

pada tahap awal, pastikan sudah install nginx dengan cara sebagai berikut
```
sudo apt update
sudo apt install nginx
```
setelah itu, dilakukan perintah berikut:
```
sudo service nginx start
```
kemudian buat docker file dan isi sebagai berikut
```
FROM nginx

RUN apt-get update && apt-get upgrade -y

EXPOSE 8080

CMD ["nginx", "-g", "daemon off;"]
```
build docker dengan perintah berikut:
```
sudo docker build -t my-image .
```
run docker
```
sudo docker run -d -p 8080:80 my-image
```
dan jika melakukan akses pada ``localhost:8080`` akan terdapat gambar berikut:

<img src="iamge/sisopmodul4individu1.jpg" width ="500">
tampilan localhost sebelum dan sesudah dilakukan pemetaan port akan sama, karena saya tidak memasukan file html di dalamnya.

## **Soal 2**
a. Pertama, buatlah Docker Network menggunakan repository https://github.com/dptsi/nginx-proxy

melakukan git clone pada repo https://github.com/dptsi/nginx-proxy
```
git clone https://github.com/dptsi/nginx-proxy
```
masuk ke direktori nginx-proxy
```
cd nginx-proxy
```
membuat docker Network
```
docker network create nginx-proxy
```
b. Lalu, pull repository https://github.com/akunlainfiqi/hbd-clone-iyok dan buatlah container-nya!

melakukan git clone pada repo https://github.com/akunlainfiqi/hbd-clone-iyok
```
git clone https://github.com/akunlainfiqi/hbd-clone-iyok
```
masuk ke direktori nginx-proxy
```
cd hbd-clone-iyok
```
lalu membuat Dockerfile berikut:
```
FROM nginx
COPY . /usr/share/nginx/html
```
dan menjalankan perintah berikut
```
docker build -t hbd-clone-iyok .
```
melakukan run dengan perintah:
```
sudo docker run -d --name hbd-clone-iyok --network nginxproxy hbd-clone-iyok
```
mengubah isi file hosts
```
sudo nano /etc/hosts
```
saya menambahkan ``127.0.0.2       hbdsayang.local`` pada file diatas

ketika ``127.0.0.2`` diakses, akan muncul tampilan di bawah

<img src="iamge/sisopmodul4individu2.jpg" width="500">
