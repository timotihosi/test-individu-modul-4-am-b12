| Nama                              | NRP        |
|-----------------------------------|------------|
|Abdullah Yasykur BM                |5025211048  |

## Test Individu Modul 4
### Soal 1

Buatlah sebuah _file_ konfigurasi **Dockerfile**. Dari file tersebut, isi sebuah _image_ dari **nginx** lalu beri informasi bahwa _image_ ini akan meng-_ekspose_ **port 80**. Build dengan nama **image-nginx versi v1.0** lalu _run_ dengan nama **container-nginx** serta me-_forward_ **port 80** dari _container_ ke **port 80** PC.

**Jawaban:**

1. Buatlah file Dockerfile yang berisi konfigurasi berikut
```Dockerfile
# Gunakan image nginx sebagai base
FROM nginx

# Ekspose port 80
EXPOSE 80

# Perintah untuk me-forward port 80 dari container ke port 80 pada PC
# (perintah ini dapat dijalankan saat container di-run)
CMD ["nginx", "-g", "daemon off;"]

```

2. melakukan build image dengan menjalankan perintah berikut di terminal:
```
docker build -t image-nginx:v1.0 .
```
| <p align="center"> build image </p> |
| -------------------------------------------- |
| <img src="https://gitlab.com/timotihosi/test-individu-modul-4-am-b12/-/blob/main/5025211035_Abdullah%20Yasykur%20BM/images/2023-06-02__4_.png" width = "400"/> |


Perintah tersebut akan menghasilkan image dengan nama image-nginx dan versi v1.0.

3. Jalankan container dengan menjalankan perintah berikut di terminal:
```
docker run -d --name container-nginx -p 80:80 image-nginx:v1.0
```
| <p align="center"> run container </p> |
| -------------------------------------------- |
| <img src="https://gitlab.com/timotihosi/test-individu-modul-4-am-b12/-/blob/main/5025211035_Abdullah%20Yasykur%20BM/images/2023-06-02__8_.png" width = "400"/> |

Perintah tersebut akan menjalankan container dengan nama container-nginx dan meneruskan (forwarding) port 80 dari container ke port 80.

akses server nginx yang berjalan di container melalui http://localhost pada browser.

4. Hasil localhost

| <p align="center"> localhost nginx </p> |
| -------------------------------------------- |
| <img src="https://gitlab.com/timotihosi/test-individu-modul-4-am-b12/-/blob/main/5025211035_Abdullah%20Yasykur%20BM/images/2023-06-02__6_.png" width = "400"/> |

### Soal 2

Pertama, buatlah Docker Network menggunakan _repository_ **https://github.com/dptsi/nginx-proxy**

Lalu, _pull repository_ **https://github.com/akunlainfiqi/hbd-clone-iyok** dan buatlah _container_-nya!

Lalu, buatlah _domain_ **hbdsayang.local** di _hosts_ agar mengarah ke _container_ tersebut!

**Jawaban:**

1. melakukan git clone untuk repo https://github.com/dptsi/nginx-proxy
```
git clone https://github.com/dptsi/nginx-proxy
```
2. masuk ke direktori nginx-proxy
```
cd nginx-proxy
```
3. membuat docker Network
```
docker network create nginx-proxy
```
| <p align="center"> nginx-proxy </p> |
| -------------------------------------------- |
| <img src="https://gitlab.com/timotihosi/test-individu-modul-4-am-b12/-/blob/main/5025211035_Abdullah%20Yasykur%20BM/images/2023-06-02__12_.png" width = "400"/> |

4. melakukan git clone untuk repo https://github.com/akunlainfiqi/hbd-clone-iyok
```
git clone https://github.com/akunlainfiqi/hbd-clone-iyok
```
5. masuk ke direktori nginx-proxy
```
cd hbd-clone-iyok
```
6. lalu membuat Dockerfile berikut:
```
FROM nginx
COPY . /usr/share/nginx/html
```
7. menjalankan perintah berikut
```
docker build -t hbd-clone-iyok .
```
| <p align="center"> hbd-clone-iyok </p> |
| -------------------------------------------- |
| <img src="https://gitlab.com/timotihosi/test-individu-modul-4-am-b12/-/blob/main/5025211035_Abdullah%20Yasykur%20BM/images/2023-06-02__13_.png" width = "400"/> |

8. melakukan run dengan perintah:
```
sudo docker run -d --name hbd-clone-iyok --network nginxproxy hbd-clone-iyok
```
9. mengubah isi file hosts
```
sudo nano /etc/hosts
```
| <p align="center"> etc/hosts </p> |
| -------------------------------------------- |
| <img src="https://gitlab.com/timotihosi/test-individu-modul-4-am-b12/-/blob/main/5025211035_Abdullah%20Yasykur%20BM/images/2023-06-02__14_.png" width = "400"/> |

dengan menambahkan ``127.0.0.2       hbdsayang.local`` pada file diatas, ketika ``127.0.0.2`` diakses, akan muncul tampilan di bawah

| <p align="center"> 127.0.0.2 nginx </p> |
| -------------------------------------------- |
| <img src="https://gitlab.com/timotihosi/test-individu-modul-4-am-b12/-/blob/main/5025211035_Abdullah%20Yasykur%20BM/images/2023-06-02__15_.png" width = "400"/> |
